fn main() {
    // 声明一个变量绑定
    let spend;

    {
        let x = 2;

        // 初始化一个绑定
        spend = x * x;
    }

    println!("spend: {}", spend);

    let spend2;

    // 报错！使用了未初始化的绑定
    // println!("spend2: {}", spend2);
    spend2 = 1;

    println!("another binding: {}", spend2);
    // spend2 = 2;
    //
    // println!("another binding: {}", spend2);

    let mut spend4 = Box::new(1);
    let spend5 = &spend4;
    spend4= Box::new(100);
    println!("{}", spend4);
    println!("{}", spend5);

    // 报错如下
    // let spend5 = &spend4;
    // ------- borrow of `spend4` occurs here
    // spend4= Box::new(100);
    // ^^^^^^ assignment to borrowed `spend4` occurs here
    // println!("{}", spend4);
    // println!("{}", spend5);
    // ------ borrow later used here

}
