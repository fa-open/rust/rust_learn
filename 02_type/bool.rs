fn main() {
  /*
  第7章 布尔类型
  Rust 使用 bool 关键字来声明一个 布尔类型 的变量。
  布尔类型 取值是 true 或 false 。 */
  let checked:bool = true;
  println!("checked {}", checked);//输出 checked true

  /*
  配置vscode进行rust debug,在.vscode/launch.json中复制如下:
  {
    // 使用 IntelliSense 了解相关属性。 
    // 悬停以查看现有属性的描述。
    // 欲了解更多信息，请访问: https://go.microsoft.com/fwlink/?linkid=830387
    "version": "0.2.0",
    "configurations": [
         {
      "type": "lldb",
      "request": "launch",
      "name": "Custom launch",
      "program": "${workspaceRoot}/bool07/target/debug/bool07",
      "cwd": "${workspaceRoot}",
    },
    ]
}
 */
}