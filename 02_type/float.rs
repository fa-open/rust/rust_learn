fn main() {
  // let price8:f64 = 99;
  // 错：mismatched types [E0308] expected `f64`, found `i32`

  let price9 = 18.00;        // 默认是 f64
  let price10:f32 = 8.88;
  let price11:f64 = 168.125;  // 双精度浮点型

  /*
  按照存储大小，把浮点型划分为 f32 和 f64。其中 f64 是默认的浮点类型。

  f32 又称为 单精度浮点型。
  f64 又称为 双精度浮点型，它是 Rust 默认的浮点类型.
  Rust 中不能将 0.0 赋值给任意一个整型，也不能将 0 赋值给任意一个浮点型。
  */
  println!("price9 {}", price9); //输出 price9 18
  println!("price10 {}", price10);//输出 price10 8.88
  println!("price11 {}", price11);//输出 price11 168.125

  let price12 =1_000_000;
  println!("price12 {}", price12); //输出 price12 1000000

  let price13 =1_000_000.666_123;
  println!("price13 {}", price13);//输出 price13 1000000.666123

  /*
  _下划线
  当数字很大的时候，Rust 可以用 **(_下划线) ** ，来让数字变得可读性更好。
  */
  let price12 =1_000_000;
  println!("price12 {}", price12); //输出 price12 1000000

  let price13 =1_000_000.666_123;
  println!("price13 {}", price13);//输出 price13 1000000.666123
}