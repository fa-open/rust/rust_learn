fn main(){
  let food = "清蒸螃蟹";  // string 字符串类型
  let price = 366;       // float 类型
  let checked = true;   // boolean 类型

  println!("food is:{}", food); //输出 food is:清蒸螃蟹
  println!("price is:{}", price);//输出 price is:366
  println!("checked is :{}", checked);//输出 checked is :true
}