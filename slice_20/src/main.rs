fn main() {
    // 切片是只向一段连续内存的指针。
    // 在 Rust 中，连续内存够区间存储的数据结构：数组(array)、字符串(string)、向量(vector)。
    // 切片可以和它们一起使用，切片也使用数字索引访问数据。下标索引从0开始。slice 可以指向数组的一部分，越界的下标会引发致命错误（panic）。
    //
    // 切片是运行时才能确定的，并不像数组那样编译时就已经确定了。
    //
    // 切片的定义
    // let 切片值 = &变量[起始位置..结束位置]
    //     [起始位置..结束位置]，这是一个左闭右开的区间。
    // 起始位置最小值是0。
    // 结束位置是数组、向量、字符串的长度。

    let mut v = Vec::new();
    v.push("Go语言极简一本通");
    v.push("Go语言微服务架构核心22讲");
    v.push("从0到Go语言微服务架构师");
    println!("len:{:?}",v.len());
    let s1= &v[1..3];
    println!("s1:{:?}",s1);
    println!("-------------------------------------------");

    //输出
    // len:3
    // s1:["Go语言微服务架构核心22讲", "从0到Go语言微服务架构师"]

    // 切片当参数
    // 切片通过引用的方式传递给函数。
    show_slice(s1);//把上面的s1传递给函数show_slice
    //输出 show_slice函数内:["Go语言微服务架构核心22讲", "从0到Go语言微服务架构师"]
    println!("-------------------------------------------");

    // 可变切片
    // 如果我们声明的原数据是可变的，同时定义切片也有**&mut**关键字，就可以更改切片元素来更改元数据了。

    // let mut v2 = Vec::new();
    // v2.push("Go语言极简一本通");
    // v2.push("Go语言微服务架构核心22讲");
    // v2.push("从0到Go语言微服务架构师");
    // println!("modify_slice之前:{:?}", v2);
    //
    // modify_slice(&mut v2[1..3]);
    // println!("modify_slice之后:{:?}", v2);

    let mut v2 = Vec::new();
    v2.push("1");
    v2.push("2");
    v2.push("3");
    println!("v2: {:?}", v2);

    modify_slice(&mut v2);
    println!("v2: {:?}", v2);


    //输出
    // modify_slice之前:["Go语言极简一本通", "Go语言微服务架构核心22讲", "从0到Go语言微服务架构师"]
    // modify_slice:["这个阶段已学习完毕", "从0到Go语言微服务架构师"]
    // modify_slice之后:["Go语言极简一本通", "这个阶段已学习完毕", "从0到Go语言微服务架构师"]
}

fn show_slice(s:&[&str]){
    println!("show_slice函数内:{:?}",s);
}

fn modify_slice(s: &mut [&str]) {
    s[0] = "这个阶段已学习完毕";
    println!("modify_slice:{:?}", s);
}
