// use std::thread::{sleep, spawn};
// use std::time::Duration;

// fn do3() {
//     for i in 1..=5 {
//         println!("do3 {}", i);
//         sleep(Duration::from_millis(500));
//     }
// }
//
// fn do4() {
//     for i in 1..=5 {
//         println!("do4 {}", i);
//         sleep(Duration::from_millis(1000));
//     }
// }

// fn main() {
//     do3();
//     do4();
// }

// fn main() {
//     let do3_spawn = spawn(do3);
//     let do4_spawn = spawn(do4);
//
//     do3_spawn.join().unwrap();
//     do4_spawn.join().unwrap();
// }

// 1. 在Cargo.toml加入async-std的依赖
// 2. 使用async-std相关的包替代std的包
use async_std::task::{sleep, spawn};
use std::time::Duration;

// 3. 方法前加async
async fn do3() {
    for i in 1..=5 {
        println!("do3 {}", i);
        // 在调用 sleep 之后添加.await。注意不是.await()调用，而是一个新语法。
        sleep(Duration::from_millis(500)).await;
    }
}

async fn do4() {
    for i in 1..=5 {
        println!("do4 {}", i);
        sleep(Duration::from_millis(1000)).await;
    }
}

// 在主函数上使用#[async_std::main]属性。
// main方法也要加async
#[async_std::main]
async fn main() {
    // 使用 spawn(do3())而不是 spawn(do3)，这表示直接调用 do3 并将结果传给 spawn。
    let do3_async = spawn(do3());
    // 对 do4()的调用增加.await。
    do4().await;
    do3_async.await;
    // 对 sleepus 不再使用 join()，而是改用.await 语法。
}
