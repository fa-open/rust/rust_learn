fn get_name() -> String {
    return String::from("Go语言微服务架构核心22讲");
}

fn get_name2() -> String {
    String::from("从0到Go语言微服务架构师")
}

fn double_price(mut price:i32){
    price=price*2;
    println!("内部的price是{}",price)
}

fn double_price2(price:&mut i32){
    // 星号（*） 用于访问变量 price 指向的内存位置上存储的变量的值。这种操作也称为 解引用。 因此 星号（*） 也称为 解引用操作符。
    *price=*price*2;
    println!("内部的price是{}",price)
}

fn show_name(name:String){
    println!("充电科目 :{}",name);
}

fn main() {
    // println!("r1:{}", get_name()); //输出 r1:Go语言微服务架构核心22讲
    // println!("r2:{}", get_name2());//输出 r2:从0到Go语言微服务架构师

    // 参数-值传递
    // 值传递 是把传递的变量的值传递给函数的 形参，所以，函数体外的变量值和函数参数是各自保存了相同的值，互不影响。因此函数内部修改函数参数的值并不会影响外部变量的值。
    let mut price=99;
    double_price(price); //输出 内部的price是198
    println!("外部的price是{}",price); //输出 外部的price是99

    // 参数-引用传递
    // 值传递变量导致重新创建一个变量。但引用传递则不会，引用传递把当前变量的内存位置传递给函数。传递的变量和函数参数都共同指向了同一个内存位置。引用传递在参数类型的前面加上 & 符号。
    let mut price=88;
    double_price2(&mut price); //输出 内部的price是176
    println!("外部的price是{}",price);//输出 外部的price是176

    let name:String = String::from("从0到Go语言微服务架构师");
    show_name(name);
    println!("调用show_name函数后: {}",name);
}
