fn main() {
    // 类型转换
    let spend = 1;
    // let cost: f64 = spend;
    // error[E0308]: mismatched types
    // 错误！不提供隐式转换

    // 可以显式转换
    let cost: f64 = spend as f64;
    println!("转换: {} -> {}", spend, cost);

    // 字面量
    // 带后缀的字面量，其类型在初始化时已经知道了。
    let x = 1u8;
    let y = 2u32;
    let z = 3f32;

    // 无后缀的字面量，其类型取决于如何使用它们。
    let i = 1;
    let f = 1.0;

    // 类型推断
    // 因为有类型说明，编译器知道类型是 u8。
    let study = String::from("从0到Go语言微服务架构师");

    // 创建一个空向量（vector，即不定长的，可以增长的数组）。
    let mut vec = Vec::new();
    // 现在编译器还不知道 `vec` 的具体类型，只知道它是某种东西构成的向量（`Vec<?>`）

    // 在向量中插入元素。
    vec.push(study);
    // 现在编译器知道 `vec` 是 String 的向量了（`Vec<String>`）。
    println!("{:?}", vec);

    //别名
    let MyU64: MyU64 = 5 as ThirdU64;
    let otherU64: OtherU64 = 2 as ThirdU64;
    println!(
        "{} MyU64 + {} OtherU64es = {} unit?",
        MyU64,
        otherU64,
        MyU64 + otherU64
    );
    // 注意类型别名并不能提供额外的类型安全，因为别名并不是新的类型。
}

// 可以用 type 语句给已有的类型取个新的名字。类型的名字必须遵循驼峰命名法（像是 CamelCase 这样），否则编译器将给出警告。原生类型是例外，比如： usize、f32，等等。别名的主要用途是避免写出冗长的模板化代码。
// type 新名字 = 原名字;
type MyU64 = u64;
type OtherU64 = u64;
type ThirdU64 = u64;
