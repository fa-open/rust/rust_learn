fn main() {
    /*
    第7章 布尔类型
Rust 使用 bool 关键字来声明一个 布尔类型 的变量。
布尔类型 取值是 true 或 false 。
    */
    let checked: bool = true;
    println!("checked {}", checked);//输出 checked true

    /*
    第 8 章 字符类型
字符(char) ，就是字符串的基本组成部分，也就是单个字符或字。
Rust 使用 UTF-8 作为底层的编码 ，而不是常见的使用 ASCII 作为底层编码。
Rust 中的 字符数据类型 包含了 数字、字母、Unicode 和 其它特殊字符。*/
    let c = 'R';
    println!("c {}", c);
}
