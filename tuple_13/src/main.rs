
fn show_tuple(tuple:(&str,&str)){
    println!("{:?}",tuple);
}

fn main() {
    let t:(&str,&str) = ("Go语言极简一本通","掌握Go语言语法，并且可以完成单体服务应用");
    println!("{:?}",t);

    println!("{}",t.0);//输出 Go语言极简一本通
    println!("{}",t.1);//输出 掌握Go语言语法，并且可以完成单体服务应用

    show_tuple(t);

    // 元组 ( tuple )解构 就是在 tuple 中的每一个元素按照顺序一个一个赋值给变量。使用 = ，让右边的 tuple 按照顺序给等号左变的变量一个一个赋值。
    let (book,target)=t;
    println!("{}",book);//输出 Go语言极简一本通
    println!("{}",target);//输出 掌握Go语言语法，并且可以完成单体服务应用
}
