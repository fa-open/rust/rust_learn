#[derive(Debug)]
struct MyNumber {
    num: i32,
}

// From trait 允许一种类型定义 “怎么根据另一种类型生成自己”，因此它提供了一种类型转换的简单机制。在标准库中有无数 From 的实现，规定原生类型及其他常见类型的转换功能。
impl From<i32> for MyNumber {
    fn from(item: i32) -> Self {
        MyNumber { num: item }
    }
}
fn main() {
    let s1 = "从0到Go语言微服务架构师";
    let s2 = String::from(s1);

    let my_number = MyNumber { num: 1 };
    println!("{:?}", my_number);

    // Into trait 就是把 From trait 倒过来而已。也就是说，如果你为你的类型实现了 From，那么同时你也就免费获得了 Into。
    // 使用 Into trait 通常要求指明要转换到的类型，因为编译器大多数时候不能推断它。不过考虑到我们免费获得了 Into，这点代价不值一提。
    let spend = 3;
    let my_spend: MyNumber = spend.into();
    println!("{:?}", my_spend);

    let cost: i32 = "5".parse().unwrap();
    println!("{}", cost);
}