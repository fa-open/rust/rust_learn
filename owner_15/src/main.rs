fn main() {
    // https://www.go-edu.cn/2022/06/05/rust-18-%E6%89%80%E6%9C%89%E6%9D%83%E5%92%8C%E7%A7%BB%E5%8A%A8/

    // 所有权
    // 所有权就是值一个东西归属谁。Rust 中一个变量对应一个值，变量就称为这个值得所有者。

    let name="从0到Go语言微服务架构师";
    // 这句话的意思就是，”从 0 到 Go 语言微服务架构师” 这个值所在内存块由变量 name 所有。
    // Rust 中，只能由一个所有者，不允许两个同时指向同一块内存区域。变量必须指向不同的内存区域。

    // 类似我们人类把一个东西送人或丢弃。
    //
    // 以下几种方式转让所有权：
    //
    // 1. 把一个变量赋值给另一个变量。

    // 栈分配的整型
    let a = 88;
    // 将 `a` *复制*到 `b`——不存在资源移动
    let b = a;
    // 两个值各自都可以使用
    println!("a {}, and b {}", a, b);

    let v1 = vec!["Go语言极简一本通","Go语言微服务架构核心22讲","从0到Go语言微服务架构师"];
    let v2 =v1;
    // println!("{:?}",v1);

    // v1 拥有堆上数据的所有权。（每次只能有一个变量对堆上数据有所有权）
    // v2=v1 v2 拥有了堆上数据的所有权。
    // v1 已经没有对数据的所有权了，所以再使用 v1 会报错。
    // 如果 Rust 检查到 2 个变量同时拥有堆上内存的所有权。会报错如上。

    // 2. 把变量传递给函数参数。
    let studyList = vec!["Go语言极简一本通","Go语言微服务架构核心22讲","从0到Go语言微服务架构师"];
    //studyList 拥有堆上数据管理权
    let studyList2 = studyList;
    // studyList 将所有权转义给了 studyList2
    show(studyList2);
    // studyList2 将所有权转让给参数 v,studyList2 不再可用。
    // println!("studyList2 {:?}",studyList2);
    //studyList2 已经不可用。

    // 3. 函数中的返回值。
    let studyList3 = vec!["Go语言极简一本通","Go语言微服务架构核心22讲","从0到Go语言微服务架构师"];
    let studyList4 = studyList3;
    let result = show2(studyList4);
    println!("result {:?}",result);
    //输出 result ["Go语言极简一本通", "Go语言微服务架构核心22讲", "从0到Go语言微服务架构师"]
}

fn show(v:Vec<&str>){
    println!("v {:?}",v)
}

fn show2(v:Vec<&str>) -> Vec<&str>{
    println!("v {:?}",v);
    return v;
}

