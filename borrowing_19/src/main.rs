fn main() {
    // Rust 中，Borrowing（借用），就是一个函数中的变量传递给另外一个函数作为参数暂时使用。也会要求函数参数离开自己作用域的时候将所有权 还给当初传递给它的变量（好借好还，再借不难嘛!）。
    //
    // &变量名  //要把参数定义的时候这样定义。
    // 例子如下:

    let studyList = vec!["Go语言极简一本通","Go语言微服务架构核心22讲","从0到Go语言微服务架构师"];
    let studyList2 =studyList;
    show(&studyList2);
    println!("studyList2:{:?}",studyList2); //我们看到，函数show使用完v2后，我们仍然可以继续使用

    println!("-------------------------------------------");

    let mut studyList3 = vec!["Go语言极简一本通","Go语言微服务架构核心22讲","从0到Go语言微服务架构师"];
    println!("studyList3:{:?}",studyList3);
    show3(&mut studyList3);
    println!("调用后，studyList3:{:?}",studyList3);

    // 如果我们要在Borrowing（借用）的时候改变其中的值：
    //
    // 变量要用mut关键字。
    // 函数参数为可变的要用 &mut 关键字。
    // 传递参数的时候，也要用 &mut 关键字。
}

fn show(v:&Vec<&str>){
    println!("v:{:?}",v)
}

fn show2(v:&Vec<&str>){
    // v[0]="第一个充电项目已完成";
    // 报错的原因：我们的这个借用不可以是可变的。那么 Rust 中，如果想要让一个变量是可变的，只能在前面加上 mut 关键字。
    println!("v:{:?}",v)
}

// 那么 Rust 中，如果想要让一个变量是可变的，只能在前面加上 mut 关键字。
fn show3(v:&mut Vec<&str>){
    v[0]="第一个充电项目已完成";
    println!("v:{:?}",v)
}
