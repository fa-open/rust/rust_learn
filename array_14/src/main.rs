fn main() {
    // let 变量名:[数据类型;数组长度]=[值1,值2,值3,...];
    let arr1:[&str;3]=["Go语言极简一本通","Go语言微服务架构核心22讲","从0到Go语言微服务架构师"];

    // let 变量名=[值1,值2,值3,...];
    let arr2=["Go语言极简一本通","Go语言微服务架构核心22讲","从0到Go语言微服务架构师"];

    // let 变量名:[数据类型;数组长度]=[默认值,数组长度];
    let arr3:[&str;3]=["";3];

    print!("{}",arr1.len());

    let mut arr2=["Go语言极简一本通","Go语言微服务架构核心22讲","从0到Go语言微服务架构师"];
    print!("{:?}\n",arr2);
    show_arr(arr2);
    print!("{:?}\n",arr2);

    let mut arr3=["Go语言极简一本通","Go语言微服务架构核心22讲","从0到Go语言微服务架构师"];
    print!("{:?}\n",arr3);
    modify_arr(&mut arr3);
    print!("{:?}\n",arr3);
}

fn show_arr(arr:[&str;3]){
    let l = arr.len();
    for i in 0..l {
        println!("充电科目: {}",arr[i]);
    }
}

// 引用传递 传递内存的地址给函数，修改数组的任何值都会修改原来的数组。
fn modify_arr(arr:&mut [&str;3]){
    let l = arr.len();
    for i in 0..l {
        arr[i]="";
    }
}
